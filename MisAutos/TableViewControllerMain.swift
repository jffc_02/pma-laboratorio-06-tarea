
import UIKit

class Auto{
    
    var id:Int
    var nombre:String
    var subtitulo:String
    var descripcion:String
    var marca:String
    var modelo:String
    var precio:Double
    
    init(_ id:Int,_ nombre:String,_ subtitulo:String,_ descripcion:String,_ marca:String,_ modelo:String,_ precio:Double){
        self.id = id
        self.nombre = nombre
        self.subtitulo = subtitulo
        self.descripcion = descripcion
        self.marca = marca
        self.modelo = modelo
        self.precio = precio
    }
}


var auto1 = Auto(1, "Nissan GTR","565 HP | Aceleración 0 a 100 km: 2.5 s","El Nissan GTR, mejor conocido como Godzilla, encabeza nuestra lista como el mejor auto deportivo: sólo tarda 2.5 segundos pasar de 0 a 100 kilómetros por hora.","Nissan","GTR",200.000)

var auto2 = Auto(2, "Bugatti Veyron","1200 HP | Aceleración 0 a 100 km: 2.5 s","Desde su lanzamiento en 2008, el Bugatti Veyron ha sido galardonado como uno de los autos deportivos de extrema calidad hasta la fecha.  Aunque lo coloquemos en el segundo puesto, en realidad es un empate con el Nissan GTR, pues ambos solo tardan 2.5 segundos en llegar a los 100km/h.","Bugatti","Veyron",210.000)

var auto3 = Auto(3, "Lamborghini Aventador Coupé","700 HP | Aceleración 0 a 100 km: 2.9 s","El Lamborghini Aventador Coupé es uno de los autos deportivos de lujo que cuenta con un motor V12 hecho a mano, balanceado naturalmente y montado longitudinalmente en posición central.","Lamborghini","Aventador Coupé",150.000)

var auto4 = Auto(4, "Koenigsegg CCX","1004 HP | Aceleración 0 a 100 km: 3.2 s","El Koenigsegg CCX rompió el récord por alcanzar los 395 km/h como velocidad máxima en el año 2005. Actualmente ya alcanza una velocidad de 402 km/h gracias a su alta tecnología e imprescindibles avances.","Koenigsegg","CCX",205.000)

var auto5 = Auto(5, "Ferrari Enzo","660 HP | Aceleración 0 a 100 km: 3.3 s","El Ferrari Enzo cuenta con un motor V-12 central trasero de 5998 cc de cilindrada con el que logra una velocidad máxima de 350 km/h.","Ferrari","Enzo",240.000)

var auto6 = Auto(6, "Pagani Zonda","555 HP | Aceleración 0 a 100 km: 3.4 s","El auto deportivo Pagani Zonda se produjo desde 1999 hasta 2011 con un motor V12 Mercedes-Benz de 6 litros que le permite alcanzar una velocidad máxima de 320 km/h. La caja de cambios es manual de 6 velocidades.","Pagani","Zonda",230.000)

var auto7 = Auto(7, "Audi R8","610 HP | Aceleración 0 a 100 km: 3.5 s","El Audi R8 cuenta con un motor V10 de 5.2 litros con el que alcanza una velocidad máxima de 330 km/h.","Audi","R8",250.000)

var auto8 = Auto(8, "Camaro SS","455 HP | Aceleración 0 a 100 km: 4 s","Este modelo de auto deportivo del Camaro es un coupé con un rediseño más agresivo: es mucho más delgado y más bajo que su antecesor. Durante su proceso de creación fue probado en un túnel de viento para perfeccionar la aerodinámica del vehículo.","Camaro","SS",300.000)

var auto9 = Auto(9, "Ford Mustang GT","435 HP | Aceleración 0 a 100 km: 4 s","De los autos deportivos más reconocidos de nuestro listado el Ford Mustang GT ocupa el puesto número 9. Tarda 4 segundos de tardanza en llegar de 0 a 100 km.","Ford Mustang","GT",280.000)

var auto10 = Auto(10, "Audi RS7 Sportback","533 HP | Aceleración 0 a 100 km: 4.2 s.","El Audi RS 7 Sportback es el perfecto ejemplo de lo que puede llegar a ofrecer una berlina cuando pasa por las manos del departamento deportivo de una marca, concentrando una imagen imponente con las prestaciones propias de un motor de 600 CV Puedes consultar aquí todos los datos del Audi RS7 Sportback","Audi","RS7 Sportback",270.000)



class TableViewControllerMain: UITableViewController {

    var Autos = [auto1,auto2,auto3,auto4,auto5,auto6,auto7,auto8,auto9,auto10]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isEditing = true
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        setEditing(false, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Autos.count
    }

    func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0 ,y: 0 ,width: newSize.width ,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.alwaysOriginal)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel?.text = Autos[indexPath.row].nombre
        let imagen = UIImage(named: "Autos/\(Autos[indexPath.row].id).jpg")
        
        cell.imageView?.layer.borderWidth = 1
        cell.imageView?.layer.borderColor = UIColor.red.cgColor
        cell.imageView?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        cell.imageView?.image = imageWithImage(image: imagen!,scaledToSize: CGSize(width: 100, height: 50))
        cell.detailTextLabel?.text =  Autos[indexPath.row].subtitulo

        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
            let botonEliminar = UITableViewRowAction(style: .normal, title: "Eliminar"){(accionesFila, indiceFila) in
            self.Autos.remove(at: indexPath.row)
            tableView.reloadData()
            }
            botonEliminar.backgroundColor = UIColor.red
    
            let botonVer = UITableViewRowAction(style: .normal, title: "Ver más"){(accionesFila, indiceFila) in
                self.performSegue(withIdentifier: "verMas", sender: self.Autos[indexPath.row])
            }
            botonVer.backgroundColor = UIColor.blue
            return[botonEliminar,botonVer]
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "verMas"{
            let autoRecibido = sender as! Auto
            let pantalla2:ViewControllerDetalles = segue.destination as! ViewControllerDetalles
            pantalla2.auto = autoRecibido
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let objectoMovido = self.Autos[fromIndexPath.row]
        Autos.remove(at: fromIndexPath.row)
        Autos.insert(objectoMovido, at: to.row)
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if (self.isEditing) {
            self.editButtonItem.title = "Hecho"
        }else{
            self.editButtonItem.title = "Editar"
        }
    }

}
