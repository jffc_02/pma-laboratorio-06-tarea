
import UIKit

class ViewControllerDetalles: UIViewController {
    
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var descripcion: UITextView!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var marca: UILabel!
    @IBOutlet weak var modelo: UILabel!
    
    var auto:Auto = Auto(1, "Nissan GTR","565 HP | Aceleración 0 a 100 km: 2.5 s","El Nissan GTR, mejor conocido como Godzilla, encabeza nuestra lista como el mejor auto deportivo: sólo tarda 2.5 segundos pasar de 0 a 100 kilómetros por hora.","Nissan","GTR",200.000)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titulo.text = auto.nombre
        imagen.image = UIImage(named: "Autos/\(auto.id).jpg")
        imagen.layer.borderWidth = 2
        imagen.layer.borderColor = UIColor.red.cgColor
        imagen.layer.cornerRadius = 25
        descripcion.text = auto.descripcion
        precio.text = "\(String(auto.precio))00"
        marca.text = auto.marca
        modelo.text = auto.modelo
    }

}
